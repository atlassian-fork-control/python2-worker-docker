FROM docker.atlassian.io/bitbucket/bitbucket-python2-app:latest

ENV WORKER_VERSION 0.5.17
ENV WORKER_PARAMS ""
ENV SUPERVISOR_VERSION 3.3.2
ENV WORKER_NUMPROCS 10

EXPOSE 8080
EXPOSE 9001

RUN pip install python-worker==$WORKER_VERSION && supervisord==$SUPERVISOR_VERSION
COPY /etc/supervisord.conf /etc/supervisord.conf

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]
